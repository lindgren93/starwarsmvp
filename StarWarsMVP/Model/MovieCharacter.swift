//
//  MovieCharacter.swift
//  StarWarsMVP
//
//  Created by Alexander Lindgren on 2019-09-30.
//  Copyright © 2019 Alexander Lindgren. All rights reserved.
//

import Foundation

struct MetaData: Decodable {
    let count: Int
    let next: String
    let results: [MovieCharacter]
}

public struct MovieCharacter: Decodable, Identifiable {
    public let id = UUID()
    let name, height, mass, hairColor: String
    let skinColor, eyeColor, birthYear: String
    let gender: Gender
    let homeworld: String
    let films, species, vehicles, starships: [String]
    let created, edited: String
    let url: String
    var isFavorite: Bool = false

    enum CodingKeys: String, CodingKey {
        case name, height, mass
        case hairColor = "hair_color"
        case skinColor = "skin_color"
        case eyeColor = "eye_color"
        case birthYear = "birth_year"
        case gender, homeworld, films, species, vehicles, starships, created, edited, url
    }
}

enum Gender: String, Decodable {
    case female = "female"
    case male = "male"
    case nA = "n/a"
}
