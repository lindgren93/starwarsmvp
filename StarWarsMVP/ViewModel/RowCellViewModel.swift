//
//  RowCellViewModel.swift
//  StarWarsMVP
//
//  Created by Alexander Lindgren on 2019-09-30.
//  Copyright © 2019 Alexander Lindgren. All rights reserved.
//

import Foundation

public class RowCellViewModel {
    private var movieCharacter: MovieCharacter
    
    public init(movieCharacter: MovieCharacter) {
        self.movieCharacter = movieCharacter
    }
    
    public func name() -> String {
        return movieCharacter.name
    }
    
    public func id() -> UUID {
        return movieCharacter.id
    }
    
    public func isFavorite() -> Bool {
        return movieCharacter.isFavorite
    }
    
}
