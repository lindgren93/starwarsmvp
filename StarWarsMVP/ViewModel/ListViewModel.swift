//
//  ListViewModel.swift
//  StarWarsMVP
//
//  Created by Alexander Lindgren on 2019-09-30.
//  Copyright © 2019 Alexander Lindgren. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

public class ListViewModel: ObservableObject {
    public let objectWillChange = PassthroughSubject<ListViewModel, Never>()
    
    private var movieCharacters: [MovieCharacter] = [MovieCharacter]() {
        willSet {
            objectWillChange.send(self)
        }
    }
    
    func load() {
        StarWarsAPI.getMovieCharacters { (result) in
            switch result {
            case .success(let movieCharacters):
                DispatchQueue.main.async {
                    self.movieCharacters = movieCharacters
                    print("Loaded: \(movieCharacters.count) Star wars characters!")
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func setFavorite(id: UUID) {
        guard let index = movieCharacters.firstIndex(where: {$0.id == id}) else { return }
        
        self.movieCharacters[index].isFavorite.toggle()
    }
    
    func movieCharactersWithFilter(_ query: String, showFavorites: Bool, sortByFirstName: Bool) -> [MovieCharacter] {
        var result = self.movieCharacters
        
        if !query.isEmpty {
            result = result.filter({"\($0.name)".contains(query)})
        }
        
        if showFavorites {
            result = result.filter({$0.isFavorite})
        }
        
        result = result.sorted(by: {
            characterSortName($0, sortByFirstName: sortByFirstName) < characterSortName($1, sortByFirstName: sortByFirstName)
        })
        
        return result
    }
    
    //Not nice. Preferably first name and last name should be split up backend instead of here.
    func characterSortName(_ movieCharacter: MovieCharacter, sortByFirstName: Bool) -> String {
        let nameComponents = movieCharacter.name.split(separator: " ")
        
        if sortByFirstName, let firstName = nameComponents.first {
            return String(firstName)
        } else if let lastName = nameComponents.last {
            return String(lastName)
        }
        
        return ""
    }
}
