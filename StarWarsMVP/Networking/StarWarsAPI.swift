//
//  StarWarsAPI.swift
//  StarWarsMVP
//
//  Created by Alexander Lindgren on 2019-09-30.
//  Copyright © 2019 Alexander Lindgren. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case badURL
    case dataNil
    case dataParsingError
}

class StarWarsAPI {
    static let RootURL = "https://swapi.co/api/"
    static let Endpoint = StarWarsAPI.RootURL + "people/"
    
    static func getMovieCharacters(completionHandler: @escaping (Result<[MovieCharacter], NetworkError>) -> Void)  {
        guard let url = URL(string: StarWarsAPI.Endpoint) else {
            completionHandler(.failure(.badURL))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                completionHandler(.failure(.dataNil))
                return
            }
            
            if let error = error {
                completionHandler(.failure(error as! NetworkError))
                return
            }
            
            let jsonDecoder = JSONDecoder()
            
            do {
                let metaData = try jsonDecoder.decode(MetaData.self, from: data)
                completionHandler(.success(metaData.results))
            } catch let error as NSError {
                print(error.debugDescription)
                completionHandler(.failure(.dataParsingError))
            }
        }
        
        task.resume()
    }
}


