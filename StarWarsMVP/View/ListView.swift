//
//  ContentView.swift
//  StarWarsMVP
//
//  Created by Alexander Lindgren on 2019-09-30.
//  Copyright © 2019 Alexander Lindgren. All rights reserved.
//

import SwiftUI
import Combine

struct ListView: View {
    @EnvironmentObject var viewModel: ListViewModel
    @State private var searchQuery: String = ""
    @State private var showFavorites: Bool = false
    @State private var sortedByFirstName: Bool = true
    
    var body: some View {
        NavigationView {
            VStack {
                SearchBar(text: self.$searchQuery)
                
                Button(action: {
                    self.sortedByFirstName.toggle()
                }) {
                    Text("Sorted by \(self.sortedByFirstName ? "first" : "last") name")
                }.padding(.bottom, 10)
                
                List {
                    ForEach(viewModel.movieCharactersWithFilter(searchQuery, showFavorites: showFavorites, sortByFirstName: sortedByFirstName)) { (movieCharacter) in
                        NavigationLink(destination: DetailView(selectedCharacter: movieCharacter)) {
                            
                             RowCellView(rowCellViewModel: RowCellViewModel(movieCharacter: movieCharacter)) { (id) in
                                 self.viewModel.setFavorite(id: id)
                             }
                         }
                    }
                }
            }
            .navigationBarTitle(Text("Star wars characters"))
            .navigationBarItems(trailing:
                Button(action: {
                    self.showFavorites.toggle()
                }, label: {
                    Image(systemName: showFavorites ? "star.fill" : "star").resizable()
                }).frame(width: 40, height: 40, alignment: .center)
            )
        }
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
