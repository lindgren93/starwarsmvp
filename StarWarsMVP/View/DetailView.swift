//
//  DetailView.swift
//  StarWarsMVP
//
//  Created by Alexander Lindgren on 2019-09-30.
//  Copyright © 2019 Alexander Lindgren. All rights reserved.
//

import Foundation
import SwiftUI

struct DetailView: View {
    let selectedCharacter: MovieCharacter
    
    var body: some View {
        VStack {
            Text("\(selectedCharacter.name)")
                .font(.title)
                .padding(.bottom, 10)
            
            Text("Height: \(selectedCharacter.height)")
            Text("Mass: \(selectedCharacter.mass)")
            Text("Hair color: \(selectedCharacter.hairColor)")
            Text("Skin color: \(selectedCharacter.skinColor)")
            Text("eye color: \(selectedCharacter.eyeColor)")
            Text("Birth year: \(selectedCharacter.birthYear)")
            Text("Gender: \(selectedCharacter.gender.rawValue)")
            Spacer()
        }
    }
}

//struct DetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailView()
//    }
//}
