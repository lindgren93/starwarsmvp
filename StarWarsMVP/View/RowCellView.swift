//
//  RowCellView.swift
//  StarWarsMVP
//
//  Created by Alexander Lindgren on 2019-09-30.
//  Copyright © 2019 Alexander Lindgren. All rights reserved.
//

import SwiftUI

struct RowCellView: View {
    var rowCellViewModel: RowCellViewModel
    var favoriteAction: ((_ id: UUID) -> Void)
    
    var body: some View {
        HStack {
            Image(systemName: (self.rowCellViewModel.isFavorite() ? "star.fill" : "star"))
                .resizable()
                .frame(width: 40, height: 40, alignment: .center)
                .padding(.trailing, 15)
                .onTapGesture {
                    self.favoriteAction(self.rowCellViewModel.id())
                }
            
            Text(rowCellViewModel.name())
        }
    }
}

//struct RowCellView_Previews: PreviewProvider {
//    static var previews: some View {
//        RowCellView()
//    }
//}
